#include <TinyGPS++.h>
#include "rgio-gps.h"

void RgioGps::begin(int baud)
{
  _ss.begin(baud);
}

void RgioGps::smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (_ss.available())
      data.encode(_ss.read());
  } while (millis() - start < ms);
}

void RgioGps::printFloat(float val, bool valid, int len, int prec)
{
  if (!valid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i = flen; i < len; ++i)
      Serial.print(' ');
  }
  smartDelay(0);
}

void RgioGps::printInt(unsigned long val, bool valid, int len)
{
  char sz[32] = "*****************";
  if (valid)
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i = strlen(sz); i < len; ++i)
    sz[i] = ' ';
  if (len > 0)
    sz[len - 1] = ' ';
  Serial.print(sz);
  smartDelay(0);
}

void RgioGps::printDateTime(TinyGPSDate &gpsDate, TinyGPSTime &gpsTime)
{
  if (!gpsDate.isValid())
  {
    Serial.print(F("********** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d ", gpsDate.month(), gpsDate.day(), gpsDate.year());
    Serial.print(sz);
  }

  if (!gpsTime.isValid())
  {
    Serial.print(F("******** "));
  }
  else
  {
    char sz[32];
    sprintf(sz, "%02d:%02d:%02d ", gpsTime.hour(), gpsTime.minute(), gpsTime.second());
    Serial.print(sz);
  }

  printInt(gpsDate.age(), gpsDate.isValid(), 5);
  smartDelay(0);
}

void RgioGps::formatDateTime(TinyGPSDate &gpsDate, TinyGPSTime &gpsTime, String &dt)
{
  // 02/14/2019 00:53:55
  char szDate[11];
  char szTime[9];

  if (gpsDate.isValid())
    sprintf(szDate, "%02d/%02d/%02d", gpsDate.month(), gpsDate.day(), gpsDate.year());

  if (gpsTime.isValid())
    sprintf(szTime, "%02d:%02d:%02d", gpsTime.hour(), gpsTime.minute(), gpsTime.second());

  dt = String(szDate) + " " + String(szTime);
}

void RgioGps::printStr(const char *str, int len)
{
  int slen = strlen(str);
  for (int i = 0; i < len; ++i)
    Serial.print(i < slen ? str[i] : ' ');
  smartDelay(0);
}

void RgioGps::printGpsData(double destLat, double destLon)
{
  Serial.println();
  Serial.println();
  Serial.println(F("sats|hdop|latitude  |longitude  |fix |date      |time    |date|alt   |course/speed/card |distance/course|card |chars|sentences|checksum |"));
  Serial.println(F("    |    |(deg)     |(deg)      |age |          |        |age |(m)   |--- from gps ---- |--- dest mi ---|     |rx   |rx       |fail     |"));
  Serial.println(F("----|----|----------|-----------|----|----------|--------|----|------|------------------|---------------|-----|-----|---------|---------|"));

  double distanceMiToDest = TinyGPSPlus::distanceBetween(data.location.lat(), data.location.lng(), destLat, destLon) / 1609.344; // meters to miles
  double courseToDest = TinyGPSPlus::courseTo(data.location.lat(), data.location.lng(), destLat, destLon);
  const char *cardinalToDest = TinyGPSPlus::cardinal(courseToDest);

  printInt(data.satellites.value(), data.satellites.isValid(), 5);
  printInt(data.hdop.value(), data.hdop.isValid(), 5);
  printFloat(data.location.lat(), data.location.isValid(), 11, 6);
  printFloat(data.location.lng(), data.location.isValid(), 12, 6);
  printInt(data.location.age(), data.location.isValid(), 5);
  printDateTime(data.date, data.time);
  printFloat(data.altitude.meters(), data.altitude.isValid(), 7, 2);
  printFloat(data.course.deg(), data.course.isValid(), 7, 2);
  printFloat(data.speed.kmph(), data.speed.isValid(), 6, 2);
  printStr(data.course.isValid() ? TinyGPSPlus::cardinal(data.course.value()) : "*** ", 6);
  printFloat(distanceMiToDest, data.location.isValid(), 9, 2);
  printFloat(courseToDest, data.location.isValid(), 7, 2);
  printStr(data.location.isValid() ? cardinalToDest : "*** ", 6);
  printInt(data.charsProcessed(), true, 6);
  printInt(data.sentencesWithFix(), true, 10);
  printInt(data.failedChecksum(), true, 9);
  Serial.println("|");

  if (millis() > 5000 && data.charsProcessed() < 10)
    Serial.println(F("No GPS data received: check wiring"));
}
