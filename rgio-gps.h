#pragma once
#include <SoftwareSerial.h>
#include <TinyGPS++.h>

class RgioGps
{
public:
  RgioGps(int rx, int tx) : _ss(rx, tx) {}

  void begin(int baud);

  void printGpsData(double destLat = 37.2372, double destLon = 115.8018);
  void smartDelay(unsigned long ms);
  void formatDateTime(TinyGPSDate &gpsDate, TinyGPSTime &gpsTime, String &dt);

  TinyGPSPlus data;

private:
  SoftwareSerial _ss;

  void printFloat(float val, bool valid, int len, int prec);
  void printInt(unsigned long val, bool valid, int len);
  void printDateTime(TinyGPSDate &gpsDate, TinyGPSTime &gpsTime);
  void printStr(const char *str, int len);
};