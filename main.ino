#include "rgio-gps.h"

const int GPS_RX_PIN = 4;
const int GPS_TX_PIN = 3;

RgioGps gps(GPS_RX_PIN, GPS_TX_PIN);

void setup()
{
  Serial.begin(115200);
  gps.begin(9600);
}

void loop()
{
  gps.printGpsData(35.652160, -97.745960);
  gps.smartDelay(1000);
}